#docker/dockerfile:1
FROM python:3.9
ARG superuser
ARG password
ARG email
ARG secret
ARG dbuser
ARG dbpassword
ARG dbname
ARG dbhost
ENV DJANGO_SUPERUSER_USERNAME=${superuser}
ENV DJANGO_SUPERUSER_PASSWORD=${password}
ENV DJANGO_SUPERUSER_EMAIL=${email}
ENV SECRET_KEY=${secret}
ENV DBUSER=${dbuser}
ENV DBPASSWORD=${dbpassword}
ENV DBNAME=${dbname}
ENV DBHOST=${dbhost}
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
EXPOSE 80
ENTRYPOINT ["./entrypoint.sh"]
