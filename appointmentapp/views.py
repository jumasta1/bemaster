from django.views import View
from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import ListView
from django.utils import timezone
from django.views.generic.detail import DetailView
from django.views.generic.edit import FormView, CreateView
from django.contrib.auth.models import User
from .forms import PostForm, CommentForm
from .models import Post
from django.urls import reverse
# Create your views here.


class IndexView(LoginRequiredMixin, View):
    login_url = '/accounts/login/'
    redirect_field_name = 'redirect_to'
    template_name = "index.html"

    def get(self, request, *args, **kwargs):
        return render(request, self.template_name)


class PostListView(LoginRequiredMixin, ListView, FormView):
    login_url = '/accounts/login/'
    model = Post
    form_class = PostForm
    queryset = Post.objects.order_by('-post_date')
    redirect_field_name = 'redirect_to'
    template_name = 'post_list.html'


class PostView(LoginRequiredMixin, DetailView, FormView):
    model = Post
    template_name = 'post_detail.html'
    form_class = CommentForm


class CreatePostView(LoginRequiredMixin, FormView):
    form_class = PostForm

    def form_valid(self, form):
        post = form.save(commit=False)
        post.user = User.objects.get(username=self.request.user)
        post.save()
        return HttpResponseRedirect(post.get_absolute_url())


class CreateCommentView(LoginRequiredMixin, FormView):
    form_class = CommentForm
    def form_valid(self, form):
        comment = form.save(commit=False)
        post_id = form['post_id'].value()
        comment.post = Post.objects.get(id = post_id)
        comment.user = User.objects.get(username=self.request.user)
        comment.save()
        return HttpResponseRedirect(reverse('post_detail', kwargs={'pk': post_id}))
