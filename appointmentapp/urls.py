from django.urls import path
from .views import IndexView, PostListView, PostView, CreatePostView,CreateCommentView


urlpatterns = [
    path('index/',IndexView.as_view(), name='index'),
    path('posts/', PostListView.as_view(), name='posts'),
    path('post/<int:pk>/', PostView.as_view(), name='post_detail'),
    path('post/add/',CreatePostView.as_view(),name='post_add'),
    path('comment/add/',CreateCommentView.as_view(), name='comment_add')
]
