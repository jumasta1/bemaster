from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse

# # Create your models here.


class Post(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post_text = models.CharField(max_length=500,unique=False)
    post_date = models.DateTimeField(auto_now_add=True,unique=False)

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.user.username + ': ' + self.post_text


class Comment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    comment_text = models.CharField(max_length=500)
    comment_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.username + ': ' + self.post.post_text + ' : ' + self.comment_text


    def get_absolute_url(self):
        return reverse('post_detail', kwargs={'pk': self.post.pk})
