from django import forms
from django.utils.translation import gettext as _
from django.contrib.auth.models import User
from django.forms import ModelForm
from .models import Post,Comment

class PostForm(ModelForm):
    class Meta:
        model = Post
        exclude = ('user',)
        fields = ['post_text']
        labels = {
            "post_text": "Тема для дискуссии"
        }


class CommentForm(ModelForm):
    post_id = forms.CharField(widget = forms.HiddenInput())
    class Meta:
        model = Comment
        exclude = ('user',)
        fields = ['comment_text']
        labels = {
            "comment_text": "Текст комментария"
        }
